import Page from "./Page.js";
import Div from "../Tags/Div.js";

export default class Homepage extends Page {
    render() {
        return this.create();

    }

    create() {
        let myDiv = new Div();
        return myDiv.render()
    }
}