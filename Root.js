import Component from "./Component.js";
import Page from "./Page/Page.js";
import Homepage from "./Page/Homepage.js";

export default class Root extends Component{

    async launch() {
        const mainContent = document.getElementById('app');
        //let my_vdom = this.buildvDom(vdom);
        let hp = new Homepage();
        mainContent.appendChild(hp.render())
        console.log(hp.render());
    }
}


const vdom = {
    tagName: "html",
    children: [
        {tagName: "head"},
        {
            tagName: "body",
            children: [
                {
                    tagName: "ul",
                    attributes: {"class": "list"},
                    children: [
                        {
                            tagName: "li",
                            attributes: {"class": "list__item"},
                            textContent: "List item"
                        }
                    ]
                }
            ]
        }
    ]
}

const vdom2 = {
    tagName: "html",
    children: [
        {tagName: "head"},
        {
            tagName: "body",
            children: [
                {
                    tagName: "ul",
                    attributes: {"class": "list"},
                    children: [
                        {
                            tagName: "li",
                            attributes: {"class": "list__item_manudt"},
                            textContent: "List item 2"
                        }
                    ]
                }
            ]
        }
    ]
}