import Component from "../Component.js";

export default class Div extends Component {
    render() {
        const s = this.create();
        const htmlObject = document.createElement('div');
        htmlObject.innerHTML = s;
        //htmlObject.getElementById("myDiv").style.marginTop = "10px";
        return htmlObject;
    }

    create() {
        return '<div id="myDiv">Helloworld!</div>';
    }
}